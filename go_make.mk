#----------------------------------------
# 可执行文件名称
#----------------------------------------
app = helloworld

#----------------------------------------
# 交叉编译设置
#----------------------------------------
set_env := :
# 嵌入式 linux 平台
# set_env += ; export CGO_ENABLED=0; export GOOS=linux; export GOARCH=arm
# windows 平台
# set_env += ; export GOOS=windows

#----------------------------------------
# 链接参数设置
#----------------------------------------
# build_time = $(shell date +'%Y-%m-%d %H:%M:%S')
# LD_FLAG = -ldflags="-s -w -X 'gateway/config.BUILD_TIME=$(build_time)'"

#----------------------------------------
# 编译后命令执行
#----------------------------------------
# script_do := cp $(exec) "d:\Program Files\app"

all: 
	$(set_env); go build $(LD_FLAG)
	$(script_do)

zip: all
	upx -9 ${app}

up: zip
	scp ${app} root@10.10.10.1:/tmp

init:
	go mod init ${app}
	go mod tidy

update:
	go mod tidy

# 定义打包的命令
pack: zip
	echo "Generate package......"

.PHONY: clean all zip up init update pack
clean:
	go clean