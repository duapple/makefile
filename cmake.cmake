# 支持的最低版本
cmake_minimum_required(VERSION 3.0)

# 项目名称
project(project_name LANGUAGES C CXX)

# 可执行文件名称
set(PROJECT_TARGET ${PROJECT_NAME})

#[[ 需要交叉编译，请在"[["中间添加“#”
# 指定交叉编译器路径
set(TOOLSCHAIN_PATH "/opt/arm-2014.05/bin/")
set(TOOLCHAIN_HOST "${TOOLSCHAIN_PATH}/arm-none-linux-gnueabi")

# 设置工具链编译器
set(TOOLCHAIN_CC "${TOOLCHAIN_HOST}-gcc")
set(TOOLCHAIN_CXX "${TOOLCHAIN_HOST}-g++")

#告诉cmake是进行交叉编译
set(CMAKE_CROSSCOMPILING TRUE)
set(CMAKE_SYSTEM_NAME "Linux")

# 定义编译器
set(CMAKE_C_COMPILER ${TOOLCHAIN_CC})
set(CMAKE_CXX_COMPILER ${TOOLCHAIN_CXX})
#]]

# 添加源文件
set(SOURCES_LIST
    main.c
)

# 头文件路径
include_directories(./)

# 链接库的路径
link_directories(${LIBRARY_OUTPUT_DIRECTORY})

# 添加编译参数
add_compile_options(-Os -g -std=c99 -fPIC)

# 设置可执行文件输出路径
set(EXECUTABLE_OUTPUT_PATH ${PROJECT_SOURCE_DIR})

# 生成 clangd 需要的 compile_commands.json
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

# 设置库文件输出路径
# set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/out/lib)
# set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${PROJECT_SOURCE_DIR}/out/lib)

# 生成可执行文件
add_executable(${PROJECT_TARGET} )

# 源文件
target_sources(${PROJECT_TARGET} PRIVATE ${SOURCES_LIST})

# 添加链接库
target_link_libraries(${PROJECT_TARGET} )

# # 添加子目录
# add_subdirectory(utils)
 
# # 生成程序后执行命令
# add_custom_command(TARGET ${PROJECT_TARGET}
#                     POST_BUILD 
#                     COMMAND ${CMAKE_COMMAND} -E echo "generate test"
#                     COMMAND ${CMAKE_COMMAND} -E copy ${EXECUTABLE_OUTPUT_PATH}/${PROJECT_TARGET} ${EXECUTABLE_OUTPUT_PATH}/test
#                     VERBATIM 
# )

# # 编译前执行命令
# add_dependencies(${PROJECT_TARGET} aaaa)
# add_custom_target(aaaa 
#     DEPENDS bbbb
#     )
# add_custom_command(OUTPUT bbbb
#                     # ALL
#                     COMMAND ${CMAKE_COMMAND} -E echo "pre build"
#                     VERBATIM 
# )
